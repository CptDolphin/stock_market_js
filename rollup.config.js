import resolve from 'rollup-plugin-node-resolve';

export default {
	input: process.env.IN,
	output: {
		file: process.env.OUT,
		format: 'iife',
		sourcemap: true,
	},
	plugins: [
		resolve({
			main: true,
			module: true,
			browser: true,
		}),
	],
};
