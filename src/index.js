// fetch('https://www.highcharts.com/samples/data/aapl-c.json').then(x => x.json()).then(console.log);
const link = (currency, period) =>
	`https://apiv2.bitcoinaverage.com/indices/global/history/${currency}USD?period=${period}&format=json`;

const array_pad_left = (array, size) => {
	if (array.length < size) {
		let i = size - array.length;
		while (i--) {
			array.unshift(null);
		}
	}
};

void (async function() {
	// const data = await fetch('https://cdn.rawgit.com/highcharts/highcharts/057b672172ccc6c08fe7dbb27fc17ebca3f5b770/samples/data/new-intraday.json').then(x => x.json());

	const results = (await Promise.all([
		fetch(link('BTC', 'alltime')).then(x => x.json()),
		fetch(link('LTC', 'alltime')).then(x => x.json()),
		fetch(link('ETH', 'alltime')).then(x => x.json()),
	]))
		.map(list => list.reverse())
		.map(list => list.map(element => {
			const lista = [
				Date.parse(element.time),
				element.open,
				element.high,
				element.low,
				element.average,
			];

			// adds one or more fake values in case some values are missing
			// not our fault - the endpoint simply returns empty strings for some values
			if (!lista.every(Boolean)) {
				const x = lista.slice(1).find(Boolean);

				// eslint-disable-next-line
				return lista.map(value => value || x);
			}

			return lista;
		}));

	const categories = results[0].map(x => Date.parse(x.time));

	// eslint-disable-next-line
	const [btc, ltc, eth] = results;
	const array_length_max = Math.max(btc.length, ltc.length, eth.length);

	array_pad_left(btc, array_length_max);
	array_pad_left(ltc, array_length_max);
	array_pad_left(eth, array_length_max);

	window.chart = Highcharts.stockChart('chart', {
		chart: {
			zoomType: 'x',
		},
		rangeSelector: {
			selected: 1
		},
		plotOptions: {
			series: {
				turboThreshold: Infinity,
			},
		},
		legend: {
			enabled: true,
		},
		title: {
			text: 'Stock Market',
		},
		xAxis: {
			type: 'datetime',
			categories,
		},
		yAxis: {
			type: 'logarithmic',
		},
		series: [
			{ name: 'slupkowe', type: 'candlestick', data: btc, },
			{ name: 'slupkowe', type: 'candlestick', data: ltc, },
			{ name: 'slupkowe', type: 'candlestick', data: eth, },
		],
	});
}());
