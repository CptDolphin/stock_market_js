// fetch('https://www.highcharts.com/samples/data/aapl-c.json').then(x => x.json()).then(console.log);
const link = (currency, period) =>
	`https://apiv2.bitcoinaverage.com/indices/global/history/${currency}USD?period=${period}&format=json`;

const array_pad_left = (array, size) => {
	if (array.length < size) {
		let i = size - array.length;
		while (i--) {
			array.unshift(null);
		}
	}
};

void (async function() {
	const [btc, ltc, eth] = (await Promise.all([
		fetch(link('BTC', 'alltime')).then(x => x.json()),
		fetch(link('LTC', 'alltime')).then(x => x.json()),
		fetch(link('ETH', 'alltime')).then(x => x.json()),
	])).map(list => list.sort((a, b) =>
		Date.parse(a.time) - Date.parse(b.time)
	)).map(list => list.map(object =>
		object
			? [Date.parse(object.time), object.average]
			: null
	));

	const array_length_max = Math.max(btc.length, ltc.length, eth.length);

	array_pad_left(btc, array_length_max);
	array_pad_left(ltc, array_length_max);
	array_pad_left(eth, array_length_max);

	window.chart = Highcharts.stockChart('chart', {
		chart: {
			type: 'line',
			zoomType: 'x',
		},
		rangeSelector: {
			selected: 1
		},
		plotOptions: {
			series: {
				turboThreshold: Infinity,
			},
		},
		legend: {
			enabled: true,
		},
		title: {
			text: 'Fruit Consumption'
		},
		xAxis: {
			type: 'datetime',
			categories: btc.map(([time]) => time),
		},
		yAxis: {
			type: 'logarithmic',
			minorTickInterval: 0.1,
		},
		series: [
			{ name: 'btc', data: btc },
			{ name: 'ltc', data: ltc },
			{ name: 'eth', data: eth },
		],
	});
}());
